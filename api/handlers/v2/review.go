package v2

import (
	"context"
	"fmt"
	"github/FIrstService/exammicro/api-gateway/genproto/review"
	l "github/FIrstService/exammicro/api-gateway/pkg/logger"
	"net/http"
	"strconv"

	// "strconv"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create Review
// @Summary      Create Review
// @Description  Creates new review
// @Tags         review
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        review   body review.ReviewReq     true  "User"
// @Success      200  {object}  review.ReviewResp
// @Router       /v2/review [post]
func (h *handlerV2) CreateReview(c *gin.Context) {
	var (
		body        review.ReviewReq
		jspbMarshal protojson.MarshalOptions
	)
	fmt.Println(body)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.ReviewService().CreateReview(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create product", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)

}

// GetReviewById
// @Summary      GetReviewById
// @Description  Get Review info by Id
// @Tags         review
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path int    true  "id"
// @Success      200  {object}  review.Review
// @Router       /v2/review/{id} [get]
func (h *handlerV2) GetReviewById(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ReviewService().GetReviewById(ctx, &review.ID{
		Id: id,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get review")
		return
	}
	c.JSON(http.StatusOK, response)

}

// GetReviewByPostId
// @Summary      GetReviewByPostId
// @Description  Get Review info by Post Id
// @Tags         review
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path int    true  "id"
// @Success      200  {object}  review.GetRewiewsRes
// @Router       /v2/reviews/{id} [get]
func (h *handlerV2) GetReviewByPostId(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")
	fmt.Println(ids)
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	fmt.Println(id)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ReviewService().GetByPostId(ctx, &review.ID{
		Id: id,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get review")
		return
	}
	c.JSON(http.StatusOK, response)

}

// UpdateReview
// @Summary      Updatereview
// @Description  Update Review
// @Tags         admin
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        post body review.Review    true "Update"
// @Success      200  {object}  review.Review
// @Router       /v2/review/update [patch]
func (h *handlerV2) UpdateReview(c *gin.Context) {
	var (
		jspbMarshal protojson.MarshalOptions
		body        review.Review
	)
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ReviewService().UpdateReview(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update customer")
		return
	}
	c.JSON(http.StatusOK, response)

}

// DeleteByCustomerId
// @Summary      DeleteByCustomerId
// @Description  Delete Review info by CustomerId
// @Tags        admin
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path int    true  "id"
// @Success      200
// @Router       /v2/review/deleted/{id} [delete]
func (h *handlerV2) DeleteByCustomerId(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ReviewService().DeleteByCustomerId(
		ctx, &review.ID{
			Id: id,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get product")
		return
	}
	c.JSON(http.StatusOK, response)
}

// DeleteByPostid
// @Summary      DeleteByPostId
// @Description  Delete Review info by CustomerId
// @Tags        admin
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path int    true  "id"
// @Success      200
// @Router       /v2/review/delete/{id} [delete]
func (h *handlerV2) DeleteByPostId(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	fmt.Println(id)
	response, err := h.serviceManager.ReviewService().DeleteByPostId(
		ctx, &review.ID{
			Id: id,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to delete by postId review")
		return
	}
	c.JSON(http.StatusOK, response)
}
