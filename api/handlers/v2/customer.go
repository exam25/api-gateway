package v2

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github/FIrstService/exammicro/api-gateway/genproto/customer"
	l "github/FIrstService/exammicro/api-gateway/pkg/logger"
	"github/FIrstService/exammicro/api-gateway/pkg/utils"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create Customer
// @Summary      Create customer
// @Description  Creates new customer
// @Tags         customer
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        customer   body customer.CustomerReq     true  "Customers"
// @Success      200  {object}  customer.CustomerResp
// @Router       /v2/customer [post]
func (h *handlerV2) CreateCustomer(c *gin.Context) {
	var (
		body        customer.CustomerReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	// a := []*customer.Address{}
	// for _, i := range body.Addresses {
	// a = append(a, &customer.Address{
	// District:   i.District,
	// Street:     i.Street,
	// Customerid: i.Customerid,
	// Id:         i.Id,
	// })

	// }
	// body.Addresses = a
	fmt.Println(body)
	response, err := h.serviceManager.CustomerService().CreateCustomer(ctx, &body)
	fmt.Println(response, err)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create customer", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)

}

// GetCustomerById
// @Summary      GetCustomerById
// @Description  Get Customer  By Id
// @Tags         customer
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path int   true  "Getting"
// @Success      200  {object}  customer.Customer
// @Router       /v2/customer/{id} [get]
func (h *handlerV2) GetCustomerByID(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	s := &customer.ID{Id: id}
	response, err := h.serviceManager.CustomerService().GetCustomerById(
		ctx, s)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get product")
		return
	}
	c.JSON(http.StatusOK, response)

}

// GetCustomerAllinfo
// @Summary      GetCustomerById
// @Description  Get Customer  By Id
// @Tags         customer
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path int   true  "Getting"
// @Success      200  {object}  customer.GetCustomer
// @Router       /v2/customers/{id} [get]
func (h *handlerV2) GetCustomerAllinfo(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.CustomerService().GetCustomerAllinfo(
		ctx, &customer.ID{
			Id: id,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get product")
		return
	}
	c.JSON(http.StatusOK, response)

}

// UpdateCustomer
// @Summary      UpdateCustomer
// @Description  Update Customer
// @Tags         admin
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        customer body customer.Customer    true "Update"
// @Success      200  {object}  customer.Customer
// @Router       /v2/update/{id} [patch]
func (h *handlerV2) UpdateByID(c *gin.Context) {
	var (
		jspbMarshal protojson.MarshalOptions
		body        customer.Customer
	)
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.CustomerService().UpdateCustomer(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update customer")
		return
	}
	c.JSON(http.StatusOK, response)

}

// DeleteDatabase
// @Summary      DeleteCustomer
// @Description  Delete Customer info by Id
// @Tags        admin
// @Security     BearerAuth
// @Accept       json
// @Produce      json
// @Param        id path int    true  "customer_id"
// @Success      200
// @Router       /v2/delete/{id} [delete]
func (h *handlerV2) DeleteInfo(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ids := c.Param("id")
	id, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.CustomerService().DeleteCustomer(
		ctx, &customer.ID{
			Id: id,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get product")
		return
	}
	c.JSON(http.StatusOK, response)
}

// @Summary list customer
// @Description lists customers
// @Tags customer
// @Security     BearerAuth
// @Accept json
// @Produce json
// @Param page query string false "query params"
// @Param limit query string false "query params"
// @Success 200 {object} customer.CustomerResp
// @Router /v2/customerlist [get]
func (h *handlerV2) GetLists(c *gin.Context) {
	queryParams := c.Request.URL.Query()
	params, errString := utils.ParseQueryParams(queryParams)
	if errString != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": errString,
		})
		h.log.Error("failed pars queryparams")
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	fmt.Println(params.Page, params.Limit)
	response, err := h.serviceManager.CustomerService().GetLists(ctx, &customer.ListReq{
		Page:  params.Page,
		Limit: params.Limit,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed insert to GetLists")
		return
	}
	c.JSON(http.StatusAccepted, response)

}
