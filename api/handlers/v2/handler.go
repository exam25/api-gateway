package v2

import (
	t "github/FIrstService/exammicro/api-gateway/api/token"
	"github/FIrstService/exammicro/api-gateway/config"
	"github/FIrstService/exammicro/api-gateway/pkg/logger"
	"github/FIrstService/exammicro/api-gateway/services"
	"github/FIrstService/exammicro/api-gateway/storage/repo"
)

type handlerV2 struct {
	log            logger.Logger
	serviceManager services.IServiceManager
	cfg            config.Config
	redis          repo.InMemorystorageI
	jwthandler     t.JWTHandler
}

// handlerV2Config ...

type HandlerV2Config struct {
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Cfg            config.Config
	Redis          repo.InMemorystorageI
	JWTHandler     t.JWTHandler
}

// New ...

func New(c *HandlerV2Config) *handlerV2 {
	return &handlerV2{
		log:            c.Logger,
		serviceManager: c.ServiceManager,
		cfg:            c.Cfg,
		redis:          c.Redis,
		jwthandler:     c.JWTHandler,
	}

}


