package models

type CreateUser struct {
	FirstName string
	LastName  string
	Email     string
	Username  string
	Password  string
}

type Customer struct {
	FirstName    string
	LastName     string
	Email        string
	Username     string
	Password     string
	Code         string
	Refreshtoken string
}

type User struct {
	FirstName string
	LastName  string
	Email     string
	Username  string
	Uuid      string
}

type Verify struct {
	Id           string
	FirstName    string
	LastName     string
	Email        string
	Username     string
	AccessToken  string
	RefreshToken string
}

type Login struct {
	Email         string
	FirstName     string
	LastName      string
	Password      string
	Username      string
	Refreshtoken  string
	Accessestoken string
	Uuid          string
}

type Admin struct {
	Email         string
	Password      string
	Username      string
	FirstName     string
	LastName      string
	AccessesToken string
}
